import com.hw.db.DAO.UserDAO;
import com.hw.db.models.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.*;

public class UserDAOTests {

    private JdbcTemplate mockJdbc;

    @BeforeEach
    void PreTestBehaviour() {
        mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);
    }

    @Test
    @DisplayName("Change method MC/DC full coverage test #1")
    void ThreadListTest1() {
        UserDAO.Change(new User("Faraday", null, null, null));
        verifyNoInteractions(mockJdbc);
    }

    @Test
    @DisplayName("Change method MC/DC full coverage test #2")
    void ThreadListTest2() {
        UserDAO.Change(new User("Faraday", null, null, "about"));
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;"), Mockito.any(String.class), Mockito.any(String.class));
    }

    @Test
    @DisplayName("Change method MC/DC full coverage test #3")
    void ThreadListTest3() {
        UserDAO.Change(new User("Faraday", null, "fullname", null));
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"), Mockito.any(String.class), Mockito.any(String.class));
    }

    @Test
    @DisplayName("Change method MC/DC full coverage test #4")
    void ThreadListTest4() {
        UserDAO.Change(new User("Faraday", "email", null, null));
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"), Mockito.any(String.class), Mockito.any(String.class));
    }

    @Test
    @DisplayName("Change method MC/DC full coverage test #5")
    void ThreadListTest5() {
        UserDAO.Change(new User("Faraday", null, "fullname", "about"));
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;"), Mockito.any(String.class), Mockito.any(String.class), Mockito.any(String.class));
    }

    @Test
    @DisplayName("Change method MC/DC full coverage test #6")
    void ThreadListTest6() {
        UserDAO.Change(new User("Faraday", "email", "fullname", null));
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;"), Mockito.any(String.class), Mockito.any(String.class), Mockito.any(String.class));
    }

    @Test
    @DisplayName("Change method MC/DC full coverage test #7")
    void ThreadListTest7() {
        UserDAO.Change(new User("Faraday", "email", null, "about"));
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;"), Mockito.any(String.class), Mockito.any(String.class), Mockito.any(String.class));
    }

    @Test
    @DisplayName("Change method MC/DC full coverage test #8")
    void ThreadListTest8() {
        UserDAO.Change(new User("Faraday", "email", "fullname", "about"));
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"), Mockito.any(String.class), Mockito.any(String.class), Mockito.any(String.class), Mockito.any(String.class));
    }

}
